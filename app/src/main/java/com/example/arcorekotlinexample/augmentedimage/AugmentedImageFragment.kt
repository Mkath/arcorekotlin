package com.example.arcorekotlinexample.augmentedimage

import android.app.ActivityManager
import android.content.Context
import android.content.res.AssetManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.ar.core.AugmentedImageDatabase
import com.google.ar.core.Config
import com.google.ar.core.Session
import com.google.ar.sceneform.ux.ArFragment
import java.io.IOException

class AugmentedImageFragment : ArFragment() {
    val TAG = "AugmentedImageFragment"
    val DEFAULT_IMAGE_NAME = "skulls_image.png"
    val MIN_OPENGL_VERSION = 3.0

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            Log.e(TAG, "Sceneform requires Android N or later")
            Toast.makeText(context,"Sceneform requires Android N or later", Toast.LENGTH_SHORT ).show()
        }

        val openGlVersionString =
            context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

        if (openGlVersionString.deviceConfigurationInfo.glEsVersion.toDouble() < MIN_OPENGL_VERSION) {
            Log.e(TAG, "Sceneform requires OpenGL ES 3.0 or later")
            Toast.makeText(context,"Sceneform requires OpenGL ES 3.0 or later", Toast.LENGTH_SHORT ).show()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        planeDiscoveryController.hide()
        planeDiscoveryController.setInstructionView(null)
        arSceneView.planeRenderer.isEnabled = false
        return view
    }


    override fun getSessionConfiguration(session: Session): Config {
        val config = super.getSessionConfiguration(session)
        config.focusMode = Config.FocusMode.AUTO

        if (!setUpAugmentedDatabase(config, session)){
            Toast.makeText(context,"Could not setup augmented image database", Toast.LENGTH_SHORT ).show()
        }
        return config
    }


    private fun setUpAugmentedDatabase(config: Config, session: Session): Boolean {
        val augmentedImageDatabase = AugmentedImageDatabase(session)

        val assetManager = if (context != null) context!!.assets else null
        if (assetManager == null)
        {
            Log.e(TAG, "Context is null, cannot intitialize image database.")
            return false
        }

        val augmentedImageBitmap = loadAugmentedImageBitmap(assetManager)
        if (augmentedImageBitmap == null)
        {
            return false
        }
        augmentedImageDatabase.addImage(DEFAULT_IMAGE_NAME, augmentedImageBitmap)
        config.augmentedImageDatabase = augmentedImageDatabase
        return true
    }

    private fun loadAugmentedImageBitmap(assetManager:AssetManager): Bitmap {
        try
        {
            assetManager.open(DEFAULT_IMAGE_NAME).use { `is`-> return BitmapFactory.decodeStream(`is`) }
        }
        catch (e:IOException) {
            Log.e(TAG, "IO exception loading augmented image bitmap.", e)
        }

      return BitmapFactory.decodeStream(null)
    }
}