package com.example.arcorekotlinexample.augmentedimage

import android.content.Context
import android.net.Uri
import android.util.Log
import com.google.android.filament.Box
import com.google.ar.core.AugmentedImage
import com.google.ar.core.Pose
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.Node
import com.google.ar.sceneform.math.Quaternion
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.rendering.ModelRenderable
import com.google.ar.sceneform.rendering.Renderable
import com.google.ar.sceneform.ux.ArFragment
import com.google.ar.sceneform.ux.TransformableNode
import java.util.concurrent.CompletableFuture
import kotlin.math.max

class AugmentedImageNode constructor(context: Context) : AnchorNode() {
    val TAG = "AugmentedImageNode"
    lateinit var mazeNode: Node
    //  var arFragment: ArFragment = arFragment

    private var mazeRenderable: CompletableFuture<ModelRenderable> = ModelRenderable.builder()
        .setSource(context, Uri.parse("12140_Skull_v3_L2.sfb"))
        .build()

    fun setImage(image: AugmentedImage) {
        if (!mazeRenderable.isDone) {
            CompletableFuture
                .allOf(mazeRenderable)
                .thenAccept { setImage(image) }.exceptionally { throwable ->
                    Log.e(TAG, "Exception loading", throwable)
                    null
                }
            return
        }


        anchor = image.createAnchor(image.centerPose)

        mazeNode = Node()

        val vector3 = Vector3(0.1f, 0.1f, 0.1f)
        mazeNode.setLocalScale(vector3)

        val vector = Vector3(0.7f, 0.2f, 0.1f)
        mazeNode.setLocalRotation(Quaternion.axisAngle(vector, 180f))

        mazeNode.setParent(this)

        mazeNode.renderable = mazeRenderable.getNow(null)

    }


}